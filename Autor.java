/*=========================================================================
  ENCABEZADO DE LA CLASE 
  =========================================================================*/
package Laboratorio7;
/**
 * CLASE: Autor.java
 * OBJETIVO: Clase que representa a un Autor con atributos basicos
 * ASIGNATURA: 
 * @version 1.0 25/07/2005
 * @author William Mendoza Rodrguez.
 */

public class Autor {

  // Atributos privados de la clase Autor
  private String nombre;
  private String paterno;
  private String materno;
  
  /**
   *
   * Constructor de un objeto autor.
   *
   */

  public Autor() {}

  /** 
   * Constructor del objeto Autor.
   * @param nombre      nombre del Autor
   * @param paterno      apellido paterno 
   * @param materno  apellido materno 
   */

  public Autor (String nombre, String paterno, String materno) {
    this.nombre = nombre;
    this.paterno = paterno;
    this.materno = materno; 
  }


  /** 
   * Analizador para el nombre.
   *
   * @return el nombre del autor
   */
  public String getNombre() { 
    return nombre; 
  }

  /** 
   * Modificador, Asigna nuevo nombre.
   *
   * @param nombre  nuevo nombre del Autor
   */
  public void setNombre (String nombre) { 
    this.nombre = nombre; 
  }
 
  /** 
   * Analizador para el apellido paterno.
   *
   * @return el apellido paterno del Autor
   */
  public String getApellidoPaterno() { 
    return paterno; 
  }

  /** 
   * Modificador, Asigna nuevo apellido paterno.
   *
   * @param paterno nuevo apellido paterno del Autor
   */
  public void setApellidoPaterno (String paterno) { 
    this.paterno = paterno; 
  }
 
  /**
   *
   * Modifica el apellido materno.
   *
   * @param materno Apellido materno.
   *
   */

  public void setApellidoMaterno(String materno){
      this.materno = materno;
  }

  /**
   *
   * Retorna el apellido materno
   *
   * @return Apellido materno.
   *
   */

  public String getApellidoMaterno(){
      return materno;
  }

  /** 
   * Retorna la informacion de este Autor como un String
   *
   * @return la informacion de este Autor
   */
  public String toString() { 
    String s ="";
    
    s += "Nombre del autor:" + nombre + " " +paterno+ " " + materno + "\n";

    return s;
  }

}

