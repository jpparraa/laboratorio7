package Laboratorio7;
import java.util.Vector;
/*=========================================================================
  ENCABEZADO DE LA CLASE 
  =========================================================================*/

/**
 * CLASE: Publicacion.java
 * OBJETIVO: Clase que representa a una Generalizacion Abstracta de Publicacion
 * ASIGNATURA: 
 * @version 1.0 25/07/2005
 * @author William Mendoza Rodrguez.
 */


/**
 * Clase que representa a una Publicacin
 * @version 1.0
 * @author William Mendoza Rodriguez
 */
 
public abstract class Publicacion
{
  private String id;
  private String titulo;
  private String editorial;
  private double costo;
  private Vector autores = new Vector();
  
  /**
   * Publicacion
   * Metodo Constructor
   * Solo d' creacion
   */
  
  public Publicacion() {}
  
  /**
   *
   * Crea un objeto Publicación.
   *
   * @param id ID de la publicación.
   * @param titulo Título de la publicación.
   *
   */

  public Publicacion(String id, String titulo){
	  this.id = id;
	  this.titulo = titulo;
	this.editorial = editorial;
    this.costo    = costo;
	autores = new Vector();
  }

  /**
   * Metodo Constructor
   * @param id ID de la publicación.
   * @param titulo de la publicacion
   * @param editorial de la publicacion 
   * @param costo Costo de la publicación.
   */
  
  public Publicacion(String id, String titulo, String editorial, double costo) {
	this.id = id;
	this.titulo    = titulo;
	this.editorial = editorial;
        this.costo    = costo;
	autores = new Vector();
    }


  /**
   * Metodo Modificador del atributo ID
   * @param id de la publicacion
   */
  
  public void setID(String id) {
  	this.id = id;
  }

  /**
   *
   * Modificador de titulo.
   *
   * @param titulo Título del Libro.
   *
   */

  public void setTitulo(String titulo){
      this.titulo = titulo;
  }

  /**
   * Metodo Modificador del atributo editorial
   * @param editorial de la publicacion
   */

  public void setEditorial(String editorial) {
  	this.editorial = editorial;
  }

  /**
   * Metodo Modificador del atributo editorial
   * @param costo de la publicacion
   */

  public void setCosto(double costo) {
  	this.costo = costo;
  }

  /**
   * Metodo analizador del atributo ID
   * @return el ID de la Publicacion
   */

  public String getID() {
  	return id;
  }

  /**
   * Metodo analizador del atributo titulo
   * @return el titulo de la Publicacion
   */

  public String getTitulo() {
  	return titulo;
  }

  /**
   * Metodo analizador del atributo editorial
   * @return el editorial de la Publicacion
   */

  public String getEditorial() {
  	return editorial;
  }

  /**
   * Metodo analizador del atributo precio
   * @return el costo de la Publicacion
   */

  public double getCosto() {
  	return costo;
  }
  
  /**
   * Metodo analizador del precio al pblico
   * @return el precio al pblico
   */

  public abstract double getPrecioPublico();

  /**
   * toString
   * @return la presentacion en un String de una Publicacion
   */

  public abstract String toString();
  
  /**
   * Metodo que asigna un Autor a una Publicacion
   * @param a Autor a asignar 
   */   

  public void asignaAutor(Autor a) {
	autores.add(a);
  }

  /**
   * Metodo que da el numero de autores de una Publicacin
   * @return numero de autores que tiene una publicacion
   */

  private int numeroAutores() {
  	return autores.size();
  }

  //IMPLEMENTE EL METODO autoresPublicacion PARA REGRESAR EN UNA CADENA
  //EL (LOS) AUTOR(ES) RECORRIENDO EL VECTOR DE ESTOS E IMPRIMA SU NOMBRE COMPLETO
  
  /**
   * Metodo que da los nombres de los autores de una Publicacin
   * @return el nombre de los autores que tiene una publicacion
   */
   
   
  public String autoresPublicacion() {
	String s ="";
	Autor autorr;
	String nombre;
	
	if(numeroAutores()!=0){
			for (int i=0; i<numeroAutores(); i++){
	    		autorr = (Autor)autores.get(i);
	    		nombre = autorr.toString();
				s += "\n" + nombre + "";
			}	
		}
		else 
			s = " - \n";
	
	return s;
  }

}
