package Laboratorio7;
/**
 * CLASE: Revista.java
 * OBJETIVO: Clase que describe una SUBCLASE Revista con ao, numero, y periodicidad.
 * ASIGNATURA: 
 * @version 1.0 25/07/2005
 * @author William Mendoza Rodrguez.
 */

public class Revista extends Publicacion {
    
    // Atributos privados de la clase Revista
    
    public int anio;
    public int numero;
    public String periodicidad;
        
	/** 
     * Constructor del objeto Revista.
     * 
     */       
        
     public Revista (){
     }   
     
    /** 
     * Constructor del objeto Revista.
     *
     * @param id        Identificador del Revista
     * @param titulo    titulo del Revista
     * @param per Periodicidad.
     */
    public Revista (String id, String titulo, String per) {
	super (id, titulo); 
	this.periodicidad = per;
	
    }
    
    /**
     *
     * Constructor de revista.
     *
     * @param id ID de la revista.
     * @param titulo Título de la revista.
     * @param editorial Editorial de la revista.
     * @param costo Costo de la revista.
     * @param anio Año de la revista.
     * @param numero Número de la revista.
     * @param periodicidad Periodicidad de la revista.
     *
     */

    public Revista(String id, String titulo, String editorial, double costo,
                   int anio, int numero, String periodicidad){
        super(id, titulo, editorial, costo);
        this.anio = anio;
        this.numero = numero;
        this.periodicidad = periodicidad;
    }

	/**
	 *
	 * Retorna el año.
	 *
	 * @return Ao.
	 */

    public int getAnio(){
	    return anio;
    }

    /**
     *
     * Modifica el año.
     *
     * @param anio Año.
     *
     */

    public void setAnio(int anio){
	    this.anio = anio;
    }
     
    /**
     *
     * Retorna el número.
     *
     * @return número.
     *
     */

    public int getNumero(){
	    return numero;
    }

    /**
     *
     * Modifica el número.
     *
     * @param numero Número
     *
     */

    public void setNumero(int numero){
	    this.numero = numero;
    }

    /** 
     * Analizador para la periodicidad del Revista.
     *
     * @return La periodicidad del Revista
     */
    public String getPeriodicidad() { 
	return periodicidad; 
	
    }
    
    /** 
     * Modificador, Asigna la periodicidad de la revista.
     *
     * @param per Periodicidad de la revista.
     */
    public void setPeriodicidad (String per) { 
	this.periodicidad = per; 
    }
      
    /**
     *
     * Caclucla y retorna el costo.
     *
     * @return Costo + 7%.
     *
     */

    public double getPrecioPublico(){
	    return getCosto() * 1.07;
    }

    /** 
     * Retorna la informacion de este Revista como un String
     *
     * @return la informacion de este Revista
     */
    public String toString() { 
	String s;
	s  = "Id    : " + getID() + "\n";
	s += "Titulo   : " + getTitulo() + "\n";
	s += "Editorial: " + getEditorial() + "\n";
	s += "Costo: " + getCosto() + "\n";
	s += "Periodicidad:" +getPeriodicidad() + "\n";
	s += "Ao:" + getAnio()+ "\n";
	s += "Numero" + getNumero()+ "\n";
	s += "Autores : " + autoresPublicacion() + "\n"; 
	
	return s;
    }
    
}
