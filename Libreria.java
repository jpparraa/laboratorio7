package Laboratorio7;
import java.util.Hashtable;
import java.util.Enumeration;
/*=========================================================================
  ENCABEZADO DE LA CLASE 
  =========================================================================*/

/**
 * CLASE: Libreria.java
 * OBJETIVO: Clase que representa a una Libreria de Publicaciones (Libros y Revistas)
 * ASIGNATURA: 
 * @version 1.0 25/07/2005
 * @author William Mendoza Rodrguez.
 */

public class Libreria {

	//atributos privados de la clase
	private Hashtable publicaciones;
	
        /**
         *
         * Constructor de Librería.
         *
         */

	public Libreria() {
		publicaciones = new Hashtable();
	}

       /**
         * Metodo que registra una publicacion en la libreria
         * @param p Publicacion a registrar 
         */   

	public void registrar(Publicacion p) {
	//HAGA LO NECESARIO PARA AGREGAR LA PUBLICACION AL CONTENEDOR
	//CON SU ID COMO LLAVE
		publicaciones.put(p.getID(), p);
		
	}

        /**
         *
         * Consulta una publicación en la Librería.
         *
         * @param id ID de la publicación.
         *
         * @return Información de la publicación.
         *
         */

        public String consultar(String id){
            Publicacion p;
            p = (Publicacion)publicaciones.get(id);
            return p.toString();
        }
       /**
         * Metodo que elimina una publicacion en la libreria
         * @param llave ID de la Publicacion a eliminar
         * @return booleano indicando si fue existosa o no su eliminacion (true - si/ false - no) 
         */   
	
	public boolean eliminar(String llave) {
		Object resultado = null;
		//HAGA LO NECESARIO PARA ELIMINAR LA PUBLICACION 
		resultado = publicaciones.remove(llave);
		if(resultado == null){
			return false;
		}else{
			return true;
		}
	}
	
        /**
         *
         * Verifica si existe una publicación en la librería.
         *
         * @param id ID de la publicación.
         *
         * @return True si sí existe, en caso contrario, false.
         *
         */

        public boolean existe(String id){
            return publicaciones.containsKey(id);
        }

	/**
	 * Retorna Los datos de todas las publicaciones de la Libreria.
	 * El objeto retornado es de tipo String.
	 * <br>Precondicion: True. 	
	 * @return Los datos de todas las publicaciones existentes en la libreria.
	 */
	
	//CREE UNA ENUMERACION QUE LE PERMITA RECORRER TODO EL CONTENEDOR (HASTABLE)
	//E INVOQUE EL METODO toString() DE CADA ELEMENTO, PARA PRESENTAR SU PRECIO PUBLICO 
	public String toString() {
	String s = "";
	Publicacion p;
	
	for(Enumeration e = publicaciones.elements();e.hasMoreElements();){
		p = (Publicacion)e.nextElement();
		s += p.toString();
	}
	
		return s;
	}






}
