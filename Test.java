package Laboratorio7;
import java.util.Scanner;
/**
 *
 * Clase de prueba.
 *
 */

public class Test{

    static Libreria libreria;
    Autor a = new Autor();

    /**
     *
     * Menú principal.
     *
     */

	public static void menu(){
		
		Scanner scan = new Scanner(System.in);
		int opt = 0;
		System.out.print("\tMenú principal\n"+
				   "0: Salir.\n"+
				   "1: Registrar Libro.\n"+
                                   "2: Registrar revista.\n"+
                                   "3: Consultar publicación.\n"+
                                   "4: Listar publicaciones.\n"+
                                   "5: Eliminar publicación.\n"+
			           "Elija una opción: ");

		try{
			opt = scan.nextInt();
		}catch(Exception e){
			System.out.println("Por favor, ingrese una opción válida.");
			menu();
		}

		switch(opt){
			case 0:
			    System.exit(0);
			    break;
			case 1:
				registrarLibro();
				break;
			case 2:
				registrarRevista();
				break;
                        case 3:
                                consultar();
                                break;
                        case 4:
                                listar();
                                break;
                        case 5: eliminar();
                                break;
			default:
				System.out.println("Por favor, ingrese una opción válida.");
				menu();
		}

	}

        /**
         *
         * Registra un libro en la librería.
         *
         */

	public static void registrarLibro(){
	     Scanner scan = new Scanner(System.in); 
	     Scanner scn = new Scanner(System.in); 

    Libro l = new Libro();
	     String id = "";
	     String titulo = "";
	     String editorial = "";
	     double costo = 0;
	     int isdn = 0;
	     int edicion = 0;
	     String opt = "";

	     try{
		System.out.print("\t\tID: ");
		 id = scan.nextLine();
	         System.out.print("\t\tTítulo: ");
		 titulo = scan.nextLine();
		 System.out.print("\t\tEditorial: ");
	         editorial = scan.nextLine();
		 System.out.print("\t\tCosto: ");
		 costo = scan.nextDouble();
	         System.out.print("\t\tISDN: ");
		 isdn = scan.nextInt();
		 System.out.print("\t\tEdicion: ");
	         edicion = scan.nextInt();

                 do{
                     l.asignaAutor(asignarAutor());
                     System.out.print("¿Hay más autores? S/n: ");
                     opt = scn.nextLine();
                 }while(opt.equalsIgnoreCase("S"));

	     }catch(NumberFormatException e){
		 System.out.println("Por favor, ingrese una opción válida.");
		 registrarLibro();
	     }

	     l.setID(id);
             l.setTitulo(titulo);
             l.setEditorial(editorial);
             l.setCosto(costo);
             l.setIsdn(isdn);
             l.setEdicion(edicion);

	     libreria.registrar(l);
             menu();
	}

        /**
         *
         * Consulta una publicación en la librería.
         *
         */

	public static void consultar(){
            Scanner scan = new Scanner(System.in);
            String id;
            System.out.print("\t\tIngrese id: ");
            id = scan.nextLine();
            if(libreria.existe(id)){
                System.out.println(libreria.consultar(id));
            }else{
                System.out.println("No se encontró la publicación.");
            }
             menu();
        }
	
        /**
         *
         * Lista todas las publicaciones de la Librería.
         *
         */

        public static void listar(){
            System.out.println(libreria.toString());
             menu();
	}

        /**
         *
         * Elimina una publicación de la librería.
         *
         */

	public static void eliminar(){
            Scanner scan = new Scanner(System.in);
            System.out.print("\t\tID: ");
            String id = scan.nextLine();
            if(libreria.eliminar(id)){
                System.out.println("\t\tProceso realizado con éxito.");
            }else{
                System.out.println("No se ha elmininado la publicación.");
            }
             menu();
	}

        /**
         *
         * Registra una revista en la librería.
         *
         */

	public static void registrarRevista(){
	     Scanner scan = new Scanner(System.in); 
	     Scanner scn = new Scanner(System.in); 

    Revista r = new Revista();
	     String id = "";
	     String titulo = "";
	     String editorial = "";
	     double costo = 0;
	     int anio = 0;
             int numero = 0;
             String periodicidad = "";
            String opt = "";
	     try{
		System.out.print("\t\tID: ");
		 id = scan.nextLine();
	         System.out.print("\t\tTítulo: ");
		 titulo = scan.nextLine();
		 System.out.print("\t\tEditorial: ");
	         editorial = scan.nextLine();
		 System.out.print("\t\tCosto: ");
		 costo = scan.nextDouble();
	         System.out.print("\t\tAño: ");
		 anio = scan.nextInt();
		 System.out.print("\t\tNúmero: ");
	         numero = scan.nextInt();
                 System.out.print("\t\tPeriodicidad: ");
                 periodicidad = scn.nextLine();

                 do{
                     r.asignaAutor(asignarAutor());
                     System.out.print("¿Hay más autores? S/n");
                     opt = scn.nextLine();
                 }while(opt.equalsIgnoreCase("S"));

	     }catch(NumberFormatException e){
		 System.out.println("Por favor, ingrese una opción válida.");
		 registrarRevista();
	     }

	     r.setID(id);
             r.setTitulo(titulo);
             r.setEditorial(editorial);
             r.setCosto(costo);
             r.setAnio(anio);
             r.setNumero(numero);
             r.setPeriodicidad(periodicidad);
	     libreria.registrar(r);
             menu();
	}

        /**
         *
         * Pide al usuario información del autor/es.
         *
         * @return Objeto autor.
         *
         */

        public static Autor asignarAutor(){
            Scanner scan = new Scanner(System.in);
            String opt = "";
            String nombre = "";
            String paterno = "";
            String materno = "";
            Autor a = new Autor();

                System.out.println("\t\t\tAutor");
                System.out.print("Nombre: ");
                nombre = scan.nextLine();
                System.out.print("Primer apellido: ");
                paterno = scan.nextLine();
                System.out.print("Segund apellido: ");
                materno = scan.nextLine();
                a.setNombre(nombre);
                a.setApellidoPaterno(paterno);
                a.setApellidoMaterno(materno);

            return a;
        }
       
        /**
         *
         * Método principal
         *
         * @param args Argumentos de línea de comandos.
         *
         */

	public static void main(String[] args){
		libreria = new Libreria();
		menu();
	}
}
