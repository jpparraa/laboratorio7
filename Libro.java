package Laboratorio7;
/**
 * CLASE: Libro.java
 * OBJETIVO: Clase que describe una SUBCLASE Libro con isdn, edicion, reimpresion
 * ASIGNATURA: 
 * @version 1.0 25/07/2005
 * @author William Mendoza Rodrguez.
 */

public class Libro extends Publicacion {
    
    // Atributos privados de la clase Libro
    
    public int isdn;
    public int edicion;
    public int reimpresion;
    public static int sreimpr = 0;
        
	/** 
     * Constructor del objeto Libro.
     * 
     */       
        
     public Libro (){
     }   
     
     /**
      *
      * Constructor de libro.
      *
      * @param id ID de la publicación.
      * @param titulo Título de la publicación.
      * @param editorial Editorial.
      * @param costo Costo del libro.
      * @param isdn ISDN del libro.
      * @param edicion.
      *
      */

     public Libro(String id, String titulo, String editorial, double costo,
		  int isdn, int edicion){
	super(id, titulo, editorial, costo);
	this.isdn = isdn;
	this.edicion = edicion;
	this.reimpresion = sreimpr;
	sreimpr++;
     }

    /** 
     * Constructor del objeto Libro.
     *
     * @param id        Identificador del Libro
     * @param titulo    titulo del Libro
     * @param isdn 		isdn del Libro			
     */
    public Libro (String id, String titulo, int isdn) {
	super (id, titulo); 
	this.isdn = isdn;
	
    }
    
    /**
     *
     * Retorna el ISDN.
     *
     * @return ISDN del libro.
     *
     */

    public int getIsdn(){
	    return isdn;
    }

    /**
     *
     * Modifica el ISDN.
     *
     * @param isdn ISDN.
     *
     */

    public void setIsdn(int isdn){
	    this.isdn = isdn;
    }

    /**
     *
     * Modifica la edición del libro.
     *
     * @param edicion Edición del Libro.
     *
     */

    public void setEdicion(int edicion){
        this.edicion = edicion;
    }

    /** 
     * Analizador para la edicion del Libro.
     *
     * @return La edicion del Libro
     */
    public int getEdicion() { 
	return edicion; 
	
    }
    
    /** 
     * Modificador, Asigna la edicion de este Libro.
     *
     * @param ed   edicion de este Libro
     */
    
    public void setfacultad (int ed) { 
	edicion = ed; 
    }

    /**
     *
     * Retorna la reimpresión.
     *
     * @return Reimpresión.
     *
     */

    public int getReimpresion(){
	    return reimpresion;
    }

    /**
     *
     * Modifica la reimpresion.
     *
     * @param reimpresion Reimpresion.
     *
     */

    public void setReimpresion(int reimpresion){
	    this.reimpresion = reimpresion;
    }

    /**
     * 
     * Calcula y retorna el precio público.
     *
     * @return Costo + 12%.
     *
     */

    public double getPrecioPublico(){
	    return getCosto() * 1.12;
    }

   /** 
     * Retorna la informacion de este Libro como un String
     *
     * @return la informacion de este Libro
     */
    
    public String toString() { 
	String s;
	s  = "Id    : " + getID() + "\n";
	s += "Titulo   : " + getTitulo() + "\n";
	s += "Editorial: " + getEditorial() + "\n";
	s += "Costo: " + getCosto() + "\n";
	s += "Isdn:" +getIsdn() + "\n";
	s += "Edicin: " +getEdicion() + "\n";
	s += "Reimpresin: " + getReimpresion()+ "\n";
 	s += "Autores : " + autoresPublicacion() + "\n"; 
	
	return s;
    }
    
}

